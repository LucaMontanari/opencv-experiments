import numpy as np
import argparse
import imutils
import time
import cv2
 
camera = cv2.VideoCapture(0)
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))

#fgbg = cv2.bgsegm.createBackgroundSubtractorMOG()
fgbg = cv2.createBackgroundSubtractorMOG2()
#fgbg = cv2.bgsegm.createBackgroundSubtractorGMG()

ok, frame = camera.read()
if not ok:
    print('Cannot read video file')
    sys.exit()

while True:
    _, frame = camera.read()
    frame = imutils.rotate(frame, angle=180)
    fgmask = fgbg.apply(frame)
    #fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
    
    cv2.imshow('frame',fgmask)
    
    # Exit if ESC pressed
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

camera.release()
cv2.destroyAllWindows()
