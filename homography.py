# import the necessary packages
import numpy as np
import imutils
import cv2

camera = cv2.VideoCapture(0)

colorLower = (24, 100, 100)
colorUpper = (44, 255, 255)

if __name__ == '__main__' :
	
	# Read destination image.
	im_dst = cv2.imread('images/front.jpg')
	# Four corners of the post-it in destination image.
	#cv2.circle(im_dst, (150, 180), 1, (255, 0, 0), -1)
	#cv2.circle(im_dst, (153, 346), 1, (255, 0, 0), -1)
	#cv2.circle(im_dst, (315, 343), 1, (255, 0, 0), -1)
	#cv2.circle(im_dst, (317, 182), 1, (255, 0, 0), -1)
	pts_dst = np.array([[150, 180],[153, 346],[315, 343],[317, 182]])
	
	"""
	# Read source image.
	im_src = cv2.imread('images/perspect.jpg')
	# Four corners of the post-it in source image
	#cv2.circle(im_src, (80, 382), 1, (255, 0, 0), -1)
	#cv2.circle(im_src, (313, 376), 1, (255, 0, 0), -1)
	#cv2.circle(im_src, (146, 343), 1, (255, 0, 0), -1)
	#cv2.circle(im_src, (306, 343), 1, (255, 0, 0), -1)
	pts_src = np.array([[80, 382],[313, 376],[146, 343],[306, 343]])
	"""
	
	while True:
		_, frame = camera.read()
		frame = imutils.rotate(frame, angle=180)

		hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
		
		mask = cv2.inRange(hsv, colorLower, colorUpper)
		mask = cv2.erode(mask, None, iterations=2)
		mask = cv2.dilate(mask, None, iterations=2)
		
		cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
			cv2.CHAIN_APPROX_SIMPLE)[-2]

		if len(cnts) > 0:
			c = max(cnts, key=cv2.contourArea)
			
			epsilon = 0.05*cv2.arcLength(c,True)
			approx = cv2.approxPolyDP(c,epsilon,True)
			#print("-------------------------------------")
		
			pts = []
			for p in approx:
				pts.append(p)
			if len(pts) != 4:
				continue
			pts = np.array(pts)
			pts = pts.reshape((-1,1,2))
			cv2.polylines(frame,[pts],True,(0,255,255))

			# Calculate Homography
			h, status = cv2.findHomography(pts, pts_dst)
			
			# Warp source image to destination based on homography
			im_out = cv2.warpPerspective(frame.copy(), h, (im_dst.shape[1],im_dst.shape[0]))
			
			# Display images
			cv2.imshow("Warped Source Image", im_out)
		
		cv2.imshow("Source Image", frame)	
		cv2.imshow("Destination Image", im_dst)

		# Exit if ESC pressed
		k = cv2.waitKey(30) & 0xff
		if k == 27:
			break
