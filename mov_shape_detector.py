import numpy as np
import argparse
import imutils
import time
import cv2
 
camera = cv2.VideoCapture(0)
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))

fgbg = cv2.bgsegm.createBackgroundSubtractorMOG()
#fgbg = cv2.createBackgroundSubtractorMOG2()

ok, frame = camera.read()
if not ok:
    print('Cannot read video file')
    sys.exit()

while True:
	_, frame = camera.read()
	frame = imutils.rotate(frame, angle=180)
	fgmask = fgbg.apply(frame)
	#fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
    
	cnts = cv2.findContours(fgmask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
	center = None

	# only proceed if at least one contour was found
	if len(cnts) > 0:
		# find the largest contour in the mask, then use
		# it to compute the minimum enclosing circle and centroid
		c = max(cnts, key=cv2.contourArea)
		
		"""
		rect = cv2.minAreaRect(c)
		box = cv2.boxPoints(rect)
		box = np.int0(box)
		im = cv2.drawContours(frame,[box],0,(0,255,255),2)
		"""
		
		epsilon = 0.05*cv2.arcLength(c,True)
		approx = cv2.approxPolyDP(c,epsilon,True)
		#print("-------------------------------------")
		
		pts = []
		for p in approx:
			pts.append(p)
			p = tuple(p[0].tolist())
			cv2.circle(frame, p, 5, (0, 255, 255), -1)
		pts = np.array(pts)
		pts = pts.reshape((-1,1,2))
		cv2.polylines(frame,[pts],True,(0,255,255))
		"""
		((x, y), radius) = cv2.minEnclosingCircle(c)
		M = cv2.moments(c)
		try:
			center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
		except:
			continue

		# only proceed if the radius meets a minimum size
		if radius > 10:
			# draw the circle and centroid on the frame,
			# then update the list of tracked points
			cv2.circle(frame, (int(x), int(y)), int(radius),
				(0, 255, 255), 2)
			cv2.circle(frame, center, 5, (0, 0, 255), -1)
		"""
	cv2.imshow('frame',frame)
	cv2.imshow('mask',fgmask)
    
	# Exit if ESC pressed
	k = cv2.waitKey(30) & 0xff
	if k == 27:
		break

camera.release()
cv2.destroyAllWindows()
