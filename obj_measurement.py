from imutils import contours
import numpy as np
import argparse
import imutils
import math
import cv2

def midpoint(ptA, ptB):
	return ((ptA[0] + ptB[0]) * 0.5, (ptA[1] + ptB[1]) * 0.5)

camera = cv2.VideoCapture(0)
#camera.set(5, 5)

colorLower = (24, 100, 100)
colorUpper = (44, 255, 255)

#fgbg = cv2.bgsegm.createBackgroundSubtractorMOG()
#fgbg = cv2.createBackgroundSubtractorMOG2()

ok, frame = camera.read()
if not ok:
    print('Cannot read video file')
    sys.exit()

while True:
	_, frame = camera.read()
	frame = imutils.rotate(frame, angle=180)
	
	#fgmask = fgbg.apply(frame)
	#cnts = cv2.findContours(fgmask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
		
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
	
	mask = cv2.inRange(hsv, colorLower, colorUpper)
	mask = cv2.erode(mask, None, iterations=2)
	mask = cv2.dilate(mask, None, iterations=2)
	
	cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)[-2]
	
	pixelsPerMetric = 173 / 7.5
	if len(cnts) > 0:
		# find the largest contour in the mask, then use
		# it to compute the minimum enclosing circle and centroid
		c = max(cnts, key=cv2.contourArea)
			
		epsilon = 0.05*cv2.arcLength(c,True)
		approx = cv2.approxPolyDP(c,epsilon,True)
		#print("-------------------------------------")
		
		pts = []
		for p in approx:
			pts.append(p)
		pts = np.array(pts)
		pts = pts.reshape((-1,1,2))
		cv2.polylines(frame,[pts],True,(0,255,255))
				
		try:
			tl, bl, br, tr = pts
			tl = tl[0]
			bl = bl[0]
			br = br[0]
			tr = tr[0]
		except:
			continue
			
		(tltrX, tltrY) = midpoint(tl, tr)
		(blbrX, blbrY) = midpoint(bl, br)
		(tlblX, tlblY) = midpoint(tl, bl)
		(trbrX, trbrY) = midpoint(tr, br)
		
		# draw the midpoints on the image
		cv2.circle(frame, (int(tltrX), int(tltrY)), 5, (255, 0, 0), -1)
		cv2.circle(frame, (int(blbrX), int(blbrY)), 5, (255, 0, 0), -1)
		cv2.circle(frame, (int(tlblX), int(tlblY)), 5, (255, 0, 0), -1)
		cv2.circle(frame, (int(trbrX), int(trbrY)), 5, (255, 0, 0), -1)
		
		cv2.line(frame, (int(tltrX), int(tltrY)), (int(blbrX), int(blbrY)),
			(255, 0, 255), 2)
		cv2.line(frame, (int(tlblX), int(tlblY)), (int(trbrX), int(trbrY)),
			(255, 0, 255), 2)
			
		# compute the Euclidean distance between the midpoints
		#dA = np.linalg.norm(np.array(tltrX, tltrY) - np.array(blbrX, blbrY))
		#dB = np.linalg.norm(np.array(tlblX, tlblY) - np.array(trbrX, trbrY))
		dA = math.sqrt((tltrX-blbrX)**2 + (tltrY-blbrY)**2)
		dB = math.sqrt((tlblX-trbrX)**2 + (tlblY-trbrY)**2)
	 
		# if the pixels per metric has not been initialized, then
		# compute it as the ratio of pixels to supplied metric (in this case, inches)
		#if pixelsPerMetric is None:
		#	pixelsPerMetric = dB / 7.5
			
		# compute the size of the object
		dimA = dA / pixelsPerMetric
		dimB = dB / pixelsPerMetric
		#print(dA, dB)
	 
		# draw the object sizes on the image
		cv2.putText(frame, "{:.1f}cm".format(dimB),
			(int(tltrX - 15), int(tltrY - 10)), cv2.FONT_HERSHEY_SIMPLEX,
			0.65, (255, 255, 255), 2)
		cv2.putText(frame, "{:.1f}cm".format(dimA),
			(int(trbrX + 10), int(trbrY)), cv2.FONT_HERSHEY_SIMPLEX,
			0.65, (255, 255, 255), 2)	

	cv2.imshow('frame',frame)
	    
	# Exit if ESC pressed
	k = cv2.waitKey(30) & 0xff
	if k == 27:
		break

camera.release()
cv2.destroyAllWindows()
