import numpy as np
import argparse
import imutils
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-f", "--filename", required=True,
	help="name of the picture")
args = ap.parse_args()

cap = cv2.VideoCapture(0)

while(True):
	ret, frame = cap.read()
	frame = imutils.rotate(frame, angle=180)

	cv2.imshow('frame', frame)
			
	k = cv2.waitKey(30) & 0xff
	if k == 27:
		cv2.imwrite('images/' + args.filename, frame)
		break
        
cap.release()
cv2.destroyAllWindows()


"""

while True:
	ok, frame = cap.read()
	#frame = cv2.flip(frame, -1) # Flip camera vertically
	
	cv2.imshow('frame',frame)
	
	#with open('images/' + args.filename, 'w') as outf:
	#	cv2.imwrite(outf, frame)
	
cap.release()
cap.destroyAllWindows()
"""
